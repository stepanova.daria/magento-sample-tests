package org.example.pages.account;

import org.example.pages.base.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AccountInfoPage extends BasePage {

    public AccountInfoPage() {
        super();
    }

    @FindBy(xpath = "//input[@id='firstname']")
    private WebElement firstNameField;

    @FindBy(xpath = "//input[@id='lastname']")
    private WebElement lastnameField;

    @FindBy(xpath = "//button[@class='action save primary']")
    private WebElement saveNameUpdatesButton;

    public MyAccountPage changeName(String newName, String newSurname) {
        firstNameField.clear();
        firstNameField.sendKeys(newName);
        lastnameField.clear();
        lastnameField.sendKeys(newSurname);
        saveNameUpdatesButton.click();
        return new MyAccountPage();
    }

}
