package org.example.pages.account;

import org.example.pages.base.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MyAccountPage extends BasePage {

    public MyAccountPage() {
        super();
    }

    @FindBy(xpath = "//div[@class='box-content']/p")
    private WebElement accountInfo;

    @FindBy(xpath = "//div[@class='box-actions']/a[@class='action edit']")
    private WebElement editNameButton;

    @FindBy(xpath = "//div[@class='message-success success message']")
    private WebElement editSuccessMessage;
    public String getNameAndEmailInfo() {
        return accountInfo.getText().replaceAll("[\\t\\n\\r]+", " ");
    }

    public String getNameOnMyInfo() {
        String info = getNameAndEmailInfo();
        String name = info.split(" ")[0] + " " + info.split(" ")[1];
        return name;
    }

    public String getEmailOnMyInfo() {
        String info = getNameAndEmailInfo();
        String email = info.split(" ")[2];
        return email;
    }

    public AccountInfoPage clickEditButton() {
        editNameButton.click();
        return new AccountInfoPage();
    }

    public boolean isEditSuccessMessageDisplayed(){
        return editSuccessMessage.isDisplayed();
    }

}
