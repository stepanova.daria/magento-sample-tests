package org.example.pages.base;

import org.example.driver.WebDriverHolder;
import org.example.pages.menu.AccountMenu;
import org.example.pages.menu.CategoryMenu;
import org.example.pages.menu.ContentMenu;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;


public class BasePage {

    public BasePage() {
        PageFactory.initElements(WebDriverHolder.getInstance().getDriver(), this);
        accountMenu = new AccountMenu();
        categoryMenu = new CategoryMenu();
        contentMenu = new ContentMenu();
    }

    private AccountMenu accountMenu;

    private CategoryMenu categoryMenu;

    private ContentMenu contentMenu;

    public AccountMenu accountMenu() {
        return accountMenu;
    }

    public CategoryMenu categoryMenu() {
        return categoryMenu;
    }

    public ContentMenu contentMenu() {
        return contentMenu;
    }

    public void setWait(WebElement element) {
        WebDriverWait wait = new WebDriverWait(WebDriverHolder.getInstance().getDriver(), Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOf(element));
    }

}
