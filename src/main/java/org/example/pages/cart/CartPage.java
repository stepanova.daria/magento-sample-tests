package org.example.pages.cart;

import org.example.driver.WebDriverHolder;
import org.example.pages.base.BasePage;
import org.example.pages.checkout.CheckoutPage;
import org.example.pages.enums.Jackets;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CartPage extends BasePage {

    public CartPage() {
        super();
    }

    @FindBy(xpath = "//a[@class='action action-delete']")
    private WebElement removeButton;

    @FindBy(xpath = "//td[@class='col item']//strong[@class='product-item-name']/a")
    private WebElement productItemName;

    @FindBy(xpath = "//tr[@class='totals sub']//span[@class='price']")
    private WebElement subTotalPrice;

    @FindBy(xpath = "//button[@class='action primary checkout']/span")
    private WebElement checkoutButton;

    public CartPage removeFromCart() {
        removeButton.click();
        return new CartPage();
    }

    public CheckoutPage clickCheckout() {
        checkoutButton.click();
        return new CheckoutPage();
    }

    public String getNameOfItemFromCart() {
        return productItemName.getText();
    }

    public double getPrice() {
        String priceStr = subTotalPrice.getText();
        String formattedPrice = priceStr.replaceAll("[^0-9.]", "");
        double price = Double.parseDouble(formattedPrice);
        return price;
    }

    public CartPage removeFromCart(int nOfItem) {
        WebDriverHolder.getInstance().getDriver().findElement(
                By.xpath("//table[@id='shopping-cart-table']/tbody[" + nOfItem + "]//a[@class='action action-delete']")).click();
        return new CartPage();
    }

    public List<String> getNamesFromCart() {
        List<WebElement> elements = WebDriverHolder.getInstance().getDriver().findElements(By.xpath("//table[@id='shopping-cart-table']//strong[@class='product-item-name']"));
        List<String> namesFromCart = new ArrayList<>();
        for (WebElement e : elements) {
            String elementName = e.getText();
            namesFromCart.add(elementName);
        }
        Collections.sort(namesFromCart);
        return namesFromCart;
    }

    public List<String> getInitialNames() {
        List<String> products = new ArrayList<>();
        products.add(Jackets.JUNO.getModel());
        products.add(Jackets.NEVE.getModel());
        products.add(Jackets.OLIVIA.getModel());
        Collections.sort(products);
        return products;
    }

}
