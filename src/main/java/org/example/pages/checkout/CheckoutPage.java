package org.example.pages.checkout;

import org.example.pages.base.BasePage;
import org.example.user.User;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class CheckoutPage extends BasePage {

    public CheckoutPage() {
        super();
    }

    @FindBy(xpath = "//input[@name='firstname']")
    private WebElement firstNameField;

    @FindBy(xpath = "//input[@name='lastname']")
    private WebElement lastNameField;

    @FindBy(xpath = "//input[@name='street[0]']")
    private WebElement streetField;

    @FindBy(xpath = "//input[@name='city']")
    private WebElement cityField;

    @FindBy(xpath = "//input[@name='postcode']")
    private WebElement postcodeField;

    @FindBy(xpath = "//input[@name='telephone']")
    private WebElement phoneField;

    @FindBy(xpath = "//select[@name='region_id']")
    private WebElement stateSelect;

    @FindBy(xpath = "//input[@value ='tablerate_bestway']")
    private WebElement shippingBestSelector;

    @FindBy(xpath = "//button[@class='button action continue primary']")
    private WebElement nextButtonCheckout;

    @FindBy(xpath = "//button[@class='action primary checkout']/span")
    private WebElement placeOrderButton;

    @FindBy(xpath = "//input[@name='billing-address-same-as-shipping']")
    private WebElement sameAsShippingAddressCheckbox;

    @FindBy(xpath = "//div[@class='page-title-wrapper']//span[@class='base']")
    private WebElement pageTitleCheckout;

    public CheckoutPage fillFields(User user) {
        firstNameField.clear();
        firstNameField.sendKeys(user.getFirstName());
        lastNameField.clear();
        lastNameField.sendKeys(user.getLastName());
        streetField.sendKeys(user.getStreetAddress());
        cityField.sendKeys(user.getCity());
        phoneField.sendKeys(user.getPhone());
        postcodeField.sendKeys(user.getZip());
        Select select = new Select(stateSelect);
        select.selectByVisibleText("Alaska");
        shippingBestSelector.click();
        nextButtonCheckout.click();
        return new CheckoutPage();
    }

    public CheckoutPage placeOrder() {
        setWait(placeOrderButton);
        placeOrderButton.click();
        return new CheckoutPage();
    }

    public String getTextOnCheckout(){
        return pageTitleCheckout.getText();
    }

}
