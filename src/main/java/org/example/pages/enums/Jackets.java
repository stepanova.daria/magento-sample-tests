package org.example.pages.enums;

public enum Jackets {

    OLIVIA("Olivia 1/4 Zip Light Jacket", 77.00),
    JUNO("Juno Jacket", 77.00),
    NEVE("Neve Studio Dance Jacket", 69.00);

    private String model;
    private double price;

    Jackets(String model, double price) {
        this.model = model;
        this.price = price;
    }

    public static Jackets fromValue(String value) {
        for (Jackets jacket : values()) {
            if (jacket.model.equals(value)) {
                return jacket;
            }
        }
        return null;
    }

    public String getModel() {
        return model;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Jackets{" +
                "model='" + model + '\'' +
                ", price=" + price +
                '}';
    }

}
