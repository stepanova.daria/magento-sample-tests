package org.example.pages.login;

import org.example.pages.base.BasePage;
import org.example.pages.main.MainPage;
import org.example.user.User;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends BasePage {

    public LoginPage() {
        super();
    }

    @FindBy(xpath = "//input[@id='email']")
    private WebElement email;

    @FindBy(xpath = "//input[@id='pass']")
    private WebElement password;

    @FindBy(xpath = "//button[@id='send2']")
    private WebElement submitLoginDataButton;

    public MainPage signIn(User user){
        email.sendKeys(user.getEmail());
        password.sendKeys(user.getPassword());
        submitLoginDataButton.click();
        return new MainPage();
    }

}
