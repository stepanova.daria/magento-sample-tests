package org.example.pages.menu;


import org.example.driver.WebDriverHolder;
import org.example.pages.account.MyAccountPage;
import org.example.pages.base.BasePage;
import org.example.pages.login.LoginPage;
import org.example.pages.main.MainPage;
import org.example.pages.registration.RegistrationPage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AccountMenu {

    public AccountMenu() {
        PageFactory.initElements(WebDriverHolder.getInstance().getDriver(), this);
    }

    @FindBy(xpath = "//li[@class='authorization-link']")
    private WebElement signInButton;

    @FindBy(xpath = "//span[@class='logged-in']")
    private WebElement greetingsForUser;

    @FindBy(xpath = "//button[@class='action switch']")
    private WebElement actionSwitch;

    @FindBy(xpath = "//div[@class='customer-menu']/ul/li[3]")
    private WebElement signOutButton;

    @FindBy(xpath = "//div[@class='customer-menu']/ul/li[1]")
    private WebElement myAccountButton;

    @FindBy(xpath = "//ul[@class='header links']/li[3]/a")
    private WebElement createAccountButton;


    public LoginPage clickSignIn() {
        signInButton.click();
        return new LoginPage();
    }

    public MyAccountPage clickMyAccount() {
        actionSwitch.click();
        myAccountButton.click();
        return new MyAccountPage();
    }

    public String checkDefaultWelcomeText() {
        new BasePage().setWait(greetingsForUser);
        return greetingsForUser.getText();
    }

    public boolean checkIfSignedOut() {
        return signInButton.isDisplayed();
    }

    public boolean checkIfSignedIn() {
        return greetingsForUser.isDisplayed();
    }

    public MainPage signOut() {
        actionSwitch.click();
        signOutButton.click();
        return new MainPage();
    }

    public RegistrationPage clickCreateAccount() {
        createAccountButton.click();
        return new RegistrationPage();
    }

}
