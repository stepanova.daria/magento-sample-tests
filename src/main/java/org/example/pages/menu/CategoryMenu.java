package org.example.pages.menu;

import org.example.driver.WebDriverHolder;
import org.example.pages.base.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CategoryMenu {

    @FindBy(xpath = "//nav[@class='navigation']")
    private WebElement baseElement;


    public CategoryMenu() {
        PageFactory.initElements(WebDriverHolder.getInstance().getDriver(), this);
    }

    public BasePage selectCategoryTopMenu(String nameOfTopCategory) {
        baseElement.findElement(By.xpath(".//ul/li/a/span[text()='" + nameOfTopCategory + "']"))
                .click();
        return new BasePage();
    }

    public BasePage selectCategoryFirstLevelSubMenu(String nameOfTopCategory, String nameOfFirstSubCategory) {
        WebElement topCategory = baseElement.findElement(By.xpath(".//ul/li/a/span[text()='" + nameOfTopCategory + "']"));
        new Actions(WebDriverHolder.getInstance().getDriver()).moveToElement(topCategory).build().perform();
        topCategory.findElement(By.xpath("//ul/li/a/span[text()='" + nameOfFirstSubCategory + "']")).click();
        return new BasePage();
    }

    public BasePage selectCategorySecondLevelSubMenu(String nameOfTopCategory, String nameOfFirstSubCategory,String nameOfSecondSubCategory) {
        WebElement topCategory = baseElement.findElement(By.xpath(".//ul/li/a/span[text()='" + nameOfTopCategory + "']"));
        new Actions(WebDriverHolder.getInstance().getDriver()).moveToElement(topCategory).build().perform();
        WebElement fistLevelCategory = topCategory.findElement(By.xpath("//ul/li/a/span[text()='" + nameOfFirstSubCategory + "']"));
        new Actions(WebDriverHolder.getInstance().getDriver()).moveToElement(fistLevelCategory).build().perform();
        fistLevelCategory.findElement(By.xpath("//ul/li/a/span[text()='" + nameOfSecondSubCategory + "']")).click();
        return new BasePage();
    }
}
