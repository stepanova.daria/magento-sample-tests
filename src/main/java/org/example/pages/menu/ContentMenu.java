package org.example.pages.menu;

import org.checkerframework.checker.units.qual.C;
import org.example.driver.WebDriverHolder;
import org.example.pages.base.BasePage;
import org.example.pages.cart.CartPage;
import org.example.pages.products.ProductPage;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ContentMenu {

    public ContentMenu() {
        PageFactory.initElements(WebDriverHolder.getInstance().getDriver(), this);
    }

    @FindBy(xpath = "//div[@class='minicart-wrapper']")
    private WebElement miniCartIcon;


    @FindBy(xpath = "//button[@id='btn-minicart-close']")
    private WebElement closeCart;

    @FindBy(xpath = "//a[@class='action viewcart']")
    private WebElement viewCartButton;

    @FindBy(xpath = "//span[@class='counter-number']")
    private WebElement cartCount;

    public ProductPage viewCart() {
        miniCartIcon.click();
        viewCartButton.click();
        return new ProductPage();
    }

    public int getCartValue() {
        new BasePage().setWait(cartCount);
        return Integer.parseInt(cartCount.getText());
    }

}
