package org.example.pages.products;

import org.example.driver.WebDriverHolder;
import org.example.pages.base.BasePage;
import org.example.pages.enums.Jackets;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ProductPage extends BasePage {

    public ProductPage() {
        super();
    }

    @FindBy(xpath = "//div[@id='option-label-size-143-item-166']")
    private WebElement size;

    @FindBy(xpath = "//div[@id='option-label-color-93-item-50']")
    private WebElement color;

    @FindBy(xpath = "//button[@id='product-addtocart-button']")
    private WebElement addToCartButton;

    @FindBy(xpath = "//div[@class='message-success success message']")
    private WebElement successMessage;

    @FindBy(xpath = "//ul[@class='items']/li/a[text()='Jackets']")
    private WebElement jacketNavigationLink;

    public ProductPage selectJacket(Jackets jackets) {
        WebDriverHolder.getInstance().getDriver().findElement(By.linkText(jackets.getModel())).click();
        return new ProductPage();
    }

    public ProductPage addToCart(Jackets jackets) {
        WebDriverHolder.getInstance().getDriver().findElement(By.linkText(jackets.getModel())).click();
        size.click();
        color.click();
        addToCartButton.click();
        setWait(successMessage);
        returnToJackets();
        return new ProductPage();
    }

    public ProductPage returnToJackets() {
        jacketNavigationLink.click();
        return new ProductPage();
    }

}
