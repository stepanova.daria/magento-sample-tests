package org.example.pages.registration;

import com.github.javafaker.Faker;
import org.example.pages.account.MyAccountPage;
import org.example.pages.base.BasePage;
import org.example.user.User;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RegistrationPage extends BasePage {
    public RegistrationPage() {
        super();
    }

    @FindBy(xpath = "//input[@id='firstname']")
    private WebElement firstNameField;

    @FindBy(xpath = "//input[@id='lastname']")
    private WebElement lastNameField;

    @FindBy(xpath = "//input[@id='email_address']")
    private WebElement emailField;

    @FindBy(xpath = "//input[@id='password']")
    private WebElement passwordField;

    @FindBy(xpath = "//input[@id='password-confirmation']")
    private WebElement passwordConfirmationField;

    @FindBy(xpath = "//button[@class='action submit primary']")
    private WebElement createAccButton;

    public MyAccountPage createAcc(User user) {
        firstNameField.sendKeys(user.getFirstName());
        lastNameField.sendKeys(user.getLastName());
        emailField.sendKeys(user.getEmail());
        passwordField.sendKeys(user.getPassword());
        passwordConfirmationField.sendKeys(user.getConfirmPassword());
        createAccButton.click();
        return new MyAccountPage();
    }
}
