package org.example.user;

import lombok.Getter;
import lombok.Setter;

@Getter
public class User {
    @Setter
    private String firstName;
    @Setter
    private String lastName;
    @Setter
    private String email;
    @Setter
    private String streetAddress;
    @Setter
    private String city;
    @Setter
    private String zip;
    @Setter
    private String phone;
    private String password = "Test!123";
    private String confirmPassword = "Test!123";

}
