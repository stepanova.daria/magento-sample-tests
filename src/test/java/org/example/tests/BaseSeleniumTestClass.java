package org.example.tests;

import com.github.javafaker.Faker;
import org.example.driver.WebDriverHolder;
import org.example.pages.main.MainPage;
import org.example.pages.registration.RegistrationPage;
import org.example.user.User;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;


public class BaseSeleniumTestClass {

    protected WebDriver driver = null;

    User user = new User();

    public void goToUrl() {
        WebDriverHolder.getInstance().getDriver().navigate().to("https://magento.softwaretestingboard.com/");
    }

    public void pause(long msec) {
        try {
            Thread.sleep(msec);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @BeforeClass
    public void generate() {
        Faker fakeData = new Faker();
        user.setFirstName(fakeData.name().firstName());
        user.setLastName(fakeData.name().lastName());
        user.setEmail(fakeData.internet().emailAddress());
        user.setCity(fakeData.address().city());
        user.setStreetAddress(fakeData.address().buildingNumber() + "" + fakeData.address().streetAddress());
        user.setZip(fakeData.address().zipCode());
        user.setPhone(fakeData.phoneNumber().cellPhone());
        goToUrl();
        new MainPage().accountMenu().clickCreateAccount();
        new RegistrationPage().createAcc(user);
        clearCookies();
    }

    @BeforeMethod
    public void login() {
        goToUrl();
        new MainPage().accountMenu()
                .clickSignIn()
                .signIn(user);
        Assert.assertTrue(new MainPage().accountMenu().checkIfSignedIn());
    }

    @AfterMethod
    public void clearCookies() {
        WebDriverHolder.getInstance().getDriver().manage().deleteAllCookies();
    }

    @AfterClass
    public void killDriver() {
        WebDriverHolder.getInstance().killDriver();
    }

}
