package org.example.tests;

import org.example.pages.account.MyAccountPage;
import org.example.pages.cart.CartPage;
import org.example.pages.checkout.CheckoutPage;
import org.example.pages.enums.Jackets;
import org.example.pages.main.MainPage;
import org.example.pages.products.ProductPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SampleTests extends BaseSeleniumTestClass {

    @Test
    public void checkCreatedAccInfo() {
        new MainPage().accountMenu().clickMyAccount();
        Assert.assertEquals(new MyAccountPage().getNameOnMyInfo(), user.getFirstName() + " " + user.getLastName());
        Assert.assertEquals(new MyAccountPage().getEmailOnMyInfo(), user.getEmail());
    }

    @Test
    public void checkGreetings() {
        Assert.assertEquals(new MyAccountPage().accountMenu().checkDefaultWelcomeText(), "Welcome, " + user.getFirstName() + " " + user.getLastName() + "!");
    }

    @Test
    public void editName() {
        new MainPage()
                .accountMenu()
                .clickMyAccount()
                .clickEditButton()
                .changeName("Test1", "Test2");
        Assert.assertTrue(new MyAccountPage().isEditSuccessMessageDisplayed());
        Assert.assertEquals(new MyAccountPage().getNameOnMyInfo(), "Test1 Test2");
        new MyAccountPage().clickEditButton().changeName(user.getFirstName(), user.getLastName());
    }

    @Test
    public void addOneItemToCart() {
        new MainPage()
                .categoryMenu()
                .selectCategorySecondLevelSubMenu("Women", "Tops", "Jackets");
        new ProductPage().addToCart(Jackets.OLIVIA);
        Assert.assertEquals(new ProductPage().contentMenu().getCartValue(), 1);
        new ProductPage().contentMenu().viewCart();
        new CartPage().removeFromCart(1);
    }

    @Test
    public void addMultipleItems() {
        new MainPage()
                .categoryMenu()
                .selectCategorySecondLevelSubMenu("Women", "Tops", "Jackets");
        new ProductPage()
                .addToCart(Jackets.JUNO)
                .addToCart(Jackets.OLIVIA)
                .addToCart(Jackets.NEVE);
        pause(3000);
        new ProductPage().contentMenu().viewCart();
        Assert.assertEquals(new CartPage().getNamesFromCart(), new CartPage().getInitialNames());
        Assert.assertEquals(new CartPage().getPrice(), Jackets.OLIVIA.getPrice() + Jackets.JUNO.getPrice() + Jackets.NEVE.getPrice());
        new CartPage()
                .removeFromCart(1)
                .removeFromCart(2)
                .removeFromCart(1);
    }

    @Test
    public void testCheckout() {
        new MainPage()
                .categoryMenu()
                .selectCategorySecondLevelSubMenu("Women", "Tops", "Jackets");
        new ProductPage()
                .addToCart(Jackets.JUNO)
                .addToCart(Jackets.OLIVIA)
                .addToCart(Jackets.NEVE);
        pause(3000);
        new ProductPage().contentMenu().viewCart();
        Assert.assertEquals(new CartPage().getNamesFromCart(), new CartPage().getInitialNames());
        new CartPage()
                .clickCheckout()
                .fillFields(user);
        pause(3000);
        new CheckoutPage().placeOrder();
        pause(3000);
        Assert.assertEquals(new CheckoutPage().getTextOnCheckout(),"Thank you for your purchase!");
    }

}
